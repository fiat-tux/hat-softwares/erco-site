+++
date = "2015-07-07T23:41:20+02:00"
draft = false
title = "Credits"

+++

Erco is built with [datalove](http://datalove.me/), [Perl](http://perl.org) and some other tools:

* the Perl web framework [Mojolicious](http://mojolicio.us). All hail the hypnotoad!
* [Twitter bootstrap](http://getbootstrap.com) to look not too ugly
* [jQuery](http://jquery.com/)
* some [Perl modules and Mojolicious plugins](https://framagit.org/luc/erco/blob/master/cpanfile)
* [Aglio](https://github.com/danielgtaylor/aglio) to build the [API](/demo/api/index.html) page
* [Hugo](http://gohugo.io) to generate this site, using [Creative Theme](https://github.com/digitalcraftsman/hugo-creative-theme) as a basis
* the ebooks of the [#Pulp collection](http://www.walrus-books.com/pulps/) from the french editor [Walrus](http://www.walrus-books.com/) to refresh my brain after coding. The versions of Erco are named after the titles of those ebooks. Special thanks to [Neil Jomunsi](http://page42.org), which allowed the use of the Walrus's orange monsters as Erco mascot.
