# Erco's site

This repository contains the files used to create [Erco's site](http://erco.xyz) with [Hugo](https://gohugo.io).

The theme is based on [Creative Theme](https://github.com/digitalcraftsman/hugo-creative-theme).

# License

This work is released under the Apache License 2.0 For more information read the [License](https://git.framasoft.org/luc/erco-site/blob/master/LICENSE).
